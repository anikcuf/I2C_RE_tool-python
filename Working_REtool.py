#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug 19 02:00:59 2023

@author: me
"""

# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
print("This package requires i2c-tools.\nA guide is available at:\n"
         "https://gitlab.com/CalcProgrammer1/OpenRGB#smbus-access-1"
               "\n\nIt also requires smbus2.\nWhich may be added to"
                              " Python with:\npip install smbus2\n")

import subprocess
import sys
import contextlib
import copy
from smbus2 import SMBus
from time import sleep

class I2C_Controller:
    
    def __new__(cls, device_name):
        try:
            str(device_name)            
        except:
            print("I2C_Controller must be created with valid device name.")
            return 
        return super(type(cls), cls).__new__(cls)
    def __init__(self, device_name):
        self.device_name = str(device_name)
        self.data = { 'device_name' : self.device_name }
    def __repr__(self):
        return "I2C_Controller for "+self.device_name

    def setup(self):
        def see_all_bus():
            subprocess.run(["i2cdetect","-l"], check=True)
            user_in = list(map(int,input("\nReferencing the numbers in the left" 
                    " column following the prefex 'i2c-', separating your entry"
                     " numbers with a single space, which busses would you like"
                                     " to see address for?\n").strip().split()))
            try:
                for buses in user_in:
                    print("\n Bus "+str(buses)+"\n")
                    subprocess.run(["i2cdetect","-y",str(buses)], check=True)
            except:
                print("\n\nInvalid entry, setup failed.\n")
                raise
        def selections():
            user_bus = int(input("\nWhich single bus would you like to access"
                                                          " addresses on?\n"))
            try:
                subprocess.run(["i2cdetect","-y",str(user_bus)], check=True)
            except:
                print("\nInvalid entry, setup failed.\n")
                raise
            user_address = int(input("\n\nEntering the value as shown in the"
                                     " preceeding table, which address would"
                                                 " you like to access?\n"),16)
            try:
                subprocess.run(["i2ctransfer","-y,",str(user_bus),"r0@"+ \
                                hex(user_address)], check=True)
            except:
                print("\nInvalid entry, setup failed.\n")
                raise
            return user_bus, user_address
        def get_user_input():
            try:
                see_all_bus()
            except:
                return
            try:
                user_bus, user_address = selections()
            except:
                return
            check = input("\n\nYou have selected bus "+str(user_bus)+" and"
                               " address "+hex(user_address)+".  Would you"
                                 " like to save these selections? [Y/n]\n")
            try:
                if check.strip()[0] in ["n","N"]:
                    get_user_input()
            except:
                pass
            try:
                self.bus = SMBus(user_bus)
                self.data.update({ "bus" : user_bus })
                self.address = user_address
                self.data.update({ "address" : hex(user_address) })
            except:
                print("Values NOT saved")
                return
            print("Values saved.")
            check = input("\n\nWould you like to specify a register? [Y/n]\n")
            try:
                if check.strip()[0] in ["n","N"]:
                    return
            except:
                pass
            try:
                user_register = int(input("\n\nExpressed in hexadecimal,"
                                                " which register?\n"),16)
                self.register = user_register
                self.data.update({ "register" : hex(user_register) })
            except:
                print("Value NOT saved.")
                return
            print("Value saved")
        get_user_input()        

    def test_register(self, register, value_range, packet, *delay):
        """
        Allows testing a specific register by sending all values in given
        inclusive range to specified register, will edit give baseline package
        and delay each write by an optional wait time.

        Parameters
        ----------
        register : int
            Specific register to test.
        value_range : list
            Inclusive range of values to check.
        packet : data packet
            Baseline packet, values for the specified register are changed.
        **delay : int
            Optional delay between subsequent writes
        """
        if len(delay) == 1:
            delay = delay[0]
        else:
            delay = 0
        mod_pckt = Data_Packet(copy_from=packet)
        self.write(mod_pckt)
        for value in range(value_range[0],value_range[-1]+1):
            mod_pckt.edit(register, value)
            sleep(delay)
            self.write(mod_pckt)
            
    def read(self, *length, **route):
        """
        Read data packet from controller.

        Parameters
        ----------
        *length : hex or int
            Length of data to be sent
        **route : hex or int
            Data associated with the following keywords:
                bus      = the I2C bus on which the controller communicates
                address  = the address of the controller
                offset   = an optional offset from the register 0x00

        Examples
        ----------
        my_controller.read(5)
            Reads 5 bytes from the saved bus, address, and 
                offset (if saved, otherwise defaults to 0).
        my_controller.read(10, bus=2, address=0x49)
            Reads 10 bytes from the designated bus, address; 
                with saved offset (if saved, otherwise defaults to 0).
        my_controller.read(5, address=0x49, offset=2)
            Reads 5 bytes from the saved bus, at address 0x49, with an 
                offset of 2 postitions after register 0x00.
        """
        if len(length) == 0:
            print('No length specified.')
            return
        address, register, offset, *bus = self.prepare_route(route)
        if len(bus) == 0:
            print(self.bus.read_i2c_block_data(address, offset, length[0]))
        else:
            bus = SMBus(bus[0])
            print(bus.read_i2c_block_data(address, offset, length[0]))        

    def write(self, *packet, **route):
        """
        Send data packet to controller.
        Protocol can recognized hexadecimal data identified by the 0x prefix 
            or integer decimal data.

        Parameters
        ----------
        *packet : hex or int
            Data to be sent
        **route : hex or int
            Data associated with the following keywords:
                bus      = the I2C bus on which the controller communicates
                address  = the address of the controller
                register = the specific register at which to begin r/w
                offset   = an optional offset from the register

        Examples
        ----------
        my_controller.write(0x00, 0, 0xFF, 255)
            Writes 0x00 0x00 0xFF 0xFF to saved bus, address, register and 
                offset (if saved, otherwise defaults to 0).
        my_controller.write(0x00, 0xFF, bus=2, address=0x49, register=0xA0)
            Writes 0x00 0xFF to the designated bus, address, and register; 
                with saved offset (if saved, otherwise defaults to 0).
        my_controller.write(0, 255, address=0x49, offset=-2)
            Writes 0x00 0xFF to the saved bus, at address 0x49, with an 
                offset of 2 postitions before the saved register.
        """
        try:
            packet = packet[0].data
        except:
            pass
        if len(packet) == 0:
            print('No packet specified.')
            return

        address, register, offset, *bus = self.prepare_route(route)

        if len(bus) == 0:
            self.bus.write_i2c_block_data(address, register+offset, packet)
        else:
            bus = SMBus(bus[0])
            bus.write_i2c_block_data(address, register+offset, packet)

    def prepare_route(self, route):
        """
        Not intended to be called by the user.
        
        Parse route for I2C transfers.
        If available, extracts saved data when not otherwise specified.

        Parameters
        ----------
        route : hex or int
            Data associated with the following keywords:
                bus      = the I2C bus on which the controller communicates
                address  = the address of the controller
                register = the specific register at which to begin r/w
                offset   = an optional offset from the register
        """
        if "bus" in route:
            bus = SMBus(route["bus"])
        elif not "bus" in self.data:
            print('"bus" is not established or specified.')
            print('To establish a bus, use the .establish_bus method')
            return
        if "address" in route:
            address = route["address"]
        else:    
            try:
                address = self.address
            except:
                print('"address" is not saved or specified.')
                return
        if "register" in route:
            register = route["register"]
        else:
            try:
                register = self.register
            except:
                print('"Resiter" is not saved or specified.')
        if "offset" in route:
            offset = route["offset"]
        else:
            try:
                offset = self.offset
            except:
                offset = 0
        
        try:
            return address, register, offset, bus
        except:
            return address, register, offset
        
    def save_data(self, **data):
        """
        Saves data.
        Saved data can be read back with:
            my_controller.data["name"]
            my_controller.report("name")
        
        Parameters
        ----------
        **data :
        name=value
            name :
                Reference name for data to be saved.
            value :
                Value of data to be saved.

        Examples
        ----------
        my_controller.save_data(name_of=some_data)
            Saves some_data referenced by "name_of".
        """        
        self.data.update(copy.deepcopy(data))
        if "offset" in data:
            print("Note, when using the write and read methods, if offset"
                   " data is saved, then specifing an offset of zero will"
                     " target the saved or specified register, otherwiese"
                            " the offset will be applied to the address.")
        if "bus" in data:
            self.bus = SMBus(data["bus"])
        if "address" in data:
            self.address = data["address"]
        if "register" in data:
            self.register = data["register"]
        if "offset" in data:
            self.offset = data["offset"]

    def report(self, *requested_data,**output_file_name):
        """
        Outputs saved data.
        
        Parameters
        ----------
        *requested_data : string(s)
            Refers to the desired data output from I2C_Controller.data
            Call method without arguments to see availble data.
        **output_file_name : string
            Optional name of output file.
            Creates file if it does not already exists.
            Appends requested data to file.
            If no output is specified, data is printed to console.

        Examples
        ----------
        my_controller.report("what", goes="where")
            Writes data what to file where
        my_controller.report("this","that" is_going="/over/here.txt")
            Writes data this and that to here.txt located in directory /over/
        my_controller.report("ALL")
            Prints all data from my_controller.data to console
        """
        if len(output_file_name) > 0:
            try:
                file_out = open(output_file_name[str([file_name for \
                            file_name in output_file_name][0])], 'a')
            except:
                print("Invalid output file name.")
                return
        else:
            file_out = sys.stdout
        if len(requested_data) == 0:
            print("Report for data may request ALL or the \
                        following established parameters:")
            for key in self.data:
                print(key)
        elif "ALL" in requested_data:
            for key in self.data:
                with contextlib.redirect_stdout(file_out):
                    print(key+" : "+str(self.data[key])+'\n')
        else:
            for key in requested_data:
                try:
                    with contextlib.redirect_stdout(file_out):
                        print(key+" : "+str(self.data[key])+'\n')
                except:
                    print(key, "not defined in", self)
        if file_out != sys.stdout:
            file_out.close()

class Data_Packet:
    
    def __init__(self, *length, **make_from):
        """
        Parameters
        ----------
        *length : int
            Produce an empty data packet of specified length.
        **make_from : list int (hexadecimal or decimal),
                      or string of space separated hexadecimal,
                      or existing data packet.
            Produce a data packet with the supplied values

        Examples
        -------
        new_packet = Data_Packet(4)
            Creates data packet with values 0 0 0 0
        new_packet = Data_Packet(data=[8,9,10,11])
            Creates data packet with values 8 9 10 11
        new_packet = Data_Packet(packet=[8, 9, 0xA, 0x0B])
            Creates data packet with values 8 9 10 11
        new_packet = Data_Packet(as_string="0x8 0x09 0xA 0x0B")
            Creates data packet with values 8 9 10 11
        new_packet = Data_Packet(from_old=old_packet)
            Creates data packet with the same values as the old one
        """
        if len(make_from) == 1:
            make_from = make_from[[packet for packet in make_from][0]]
            if type(make_from) == type(self):
                make_from = copy.deepcopy(make_from.data)
            elif type(make_from) == str:
                make_from = [int(byte,16) for byte in make_from.split()]
            else:
                try:
                    make_from = list(make_from)
                except:
                    print("Invalid data structure provided.")
                    return
            self.data = make_from

        elif not type(length[0]) == int:
            print("New empty data packets must be created with a length.")
            return
        else:
            self.data  = [0]*length[0]
    def __repr__(self):
        hex_out = ""
        for byte in self.data:
            try:
                hex_out+=str(hex(byte))+" "
            except:
                hex_out+=str(byte)+" "
        return hex_out
    
    def __sub__(self,other):
        """
        Determine the differences between two data packets.
        Produces a dictionary associating the packet indicies with
            tuples showing the values of the two supplied operands.
        """
        if type(other) != type(self):
            print("Subtraction only defined between data packets.")
            return
        elif len(self.data) != len(other.data):
            print("Both data packets must be of the same length.")
            return
        else:
            differences = {}
            for index in range(len(self.data)):
                if self.data[index] != other.data[index]:
                    differences.update({index: (self.data[index],\
                                              other.data[index])})
            return differences
    def __len__(self):
        return len(self.data)

    def __getitem__(self, key):
        if isinstance(key, slice):
            out = []
            start, stop, step = key.indices(len(self.data))
            for index in range(start, stop, step):
                if type(self.data[index]) == int:
                    out.append(hex(self.data[index]))
                else:
                    out.append(self.data[index])
            return out
        elif isinstance(key, int):
            if type(self.data[key]) == int:
                return hex(self.data[key])
            else:
                return self.data[key]
    
    def index(self, value):
        return self.data.index(value)

    def edit(self,register,value):
        """
        Allows editing of data packets.
        Given a register or list of registers and a value of list of values.

        Parameters
        ----------
        register : int, list, tuple
            The register(s) to be changed.
        value : int, list, tuple
            The value(s) which the register(s) should be changed to.
        """
        if type(register) in (list, tuple):
            if type(value) in (list, tuple):
                if len(register) == len(value):
                    for index in range(len(register)):
                        self.data[register[index]]=value[index]                   
                elif len(value) == 0:
                    for index in range(len(register)):
                        self.data[register[index]]=value[0]
                else:
                    print("Number of registers and values not equal.")
                    return
            elif type(value) == int:
                for index in range(len(register)):
                    self.data[register[index]]=value
            else:
                print("Supplied value variable unsupported type.")
                return
        elif type(value) in (list, tuple):
            print("Supplied registers and values of incompatible types")
            return
        elif (type(register) == int) and (type(value) == int):
            self.data[register]=value
        else:
            print("Supplied register variable unsupported type.")
            return

def guide():

    print("Begin by creating a controller of the 'I2C_Controller' class by"
"executing:\nmy_controller = I2C_Controller('my_card')\n\nNow you may call"
" the controller setup method with:\nmy_controller.setup()\n\nThis will walk y"
"ou through process of selecting an I2C bus, an address on that bus, and offer"
" the option to specify a register to begin writes to the address from.  All v"
"alues established in setup will be written to the my_controller.data dictiona"
"ry as well as applied to their own attributes within the class, .address and "
".register are both integers, .bus is an SMBus object.  Values may be easily u"
"pdated with the .save_data method which accepts flexible inputs as keyword ar"
"guments.  For example:\nmy_controller.save_data(new=stuff)\n\nWill add the ke"
"y 'new' to the dictionary with the associated value stuff.  Using save_data w"
"ith already known keywords will overwrite the existing values.  For keys asso"
"ciated with I2C commands, they will also have their attributes overwritteen. "
" These particular keys are:\nbus, address, register, offset\n\nThe dictionary"
" can be accessed directly as a class attribute.  I"
"t can also be accessed throught the .report method.  This allows one to write"
" the dictionary to a file, or to print to console.  It takes an optional list"
" of keys as input, and an optional file.  If no key is supplied the user is p"
"rompted to use the input 'ALL' to reproduce the entire dictionary.  If no fil"
"e is supplied the report will print to console.  It is recommended, but not p"
"rohibited, that the key 'ALL' not be otherwise associated with the dictionary"
".\n\nReading from and writing to the I2c bus are accomplished with the .read "
"and .write methods.  If .setup has been completed, these require only a data "
"length (for read) and a data packet (for write).  Data packets are lists of i"
"nteger values expressed either in decimal or in hexadecimal with the 0x desig"
"nator.  A class exists for creation and management of data packets.  And data"
" packets may be saved to the dictionary.\n\nA packet of the 'Data_Packet' cla"
"ss my be created by executing:\ncommand = Data_Packet(length)\ncommand = Data"
"_Packet(copied_from=other_command)\n\nInputing length will produce a zero val"
"ue packet of the specified length, the naming of the copied_from option is fl"
"exible, this just copies the data from the supplied other_command.  Notes may"
" be freely added to the packet by creating a new attribute on the object, suc"
"h as:\ncommand.this_does = that_thing\n\nSuch that executing\ncommand.this_do"
"es\n\nWill yield an output reminding what that thing is.  The class __repr__"
" is overloaded such that printing it shows hexadecimal format of the data.  T"
"his means that my_controller.report will not convey notes on saved packets."
"\n\nData_Packet subtraction is overloaded to allow for comparing differences"
"between data packets.\n\nI2C_Controller class includes a method to test the f"
"function of a specified register by looping through writes within a given ran"
"ge.")
    return

if __name__ == "__main__":
    print("To display the user guide execute the function:\nguide()")