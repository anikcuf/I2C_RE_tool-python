I made this to hopefully streamline reverse engineering I2C RGB controllers... bear in mind I wouldn't even consider myself competent or experienced in the process. Mostly I was just getting tired of referencing notes constantly and copy pasting commands and data packets. So... on to what this does:

This will create two Python classes: I2C_Controller and Data_Packet. Data_Packet is much simpler, so I'll start there.

Data_Packet just is basically just a list holding the byte values of a packet to be written to the I2C device. It has a couple useful features. Printing a Data_Packet will show the list of bytes in hexadecimal, similar to the manner in which they are saved in a log by NvAPISpy. Subtracting two Data_Packets will produce a dictionary of bytes that are different between the two.

A Data_Packet is created by giving the constructor a list of bytes, a text string of hexadecimal bytes, or an existing Data_Packet to copy.

A Data_Packet object can have essentially any attribute added to it on the fly, allowing notes to be added with high flexibility; without changing the essential data, or the way in which it is used. Speaking of, a Data_Packet can be used by an I2C_Controller just like a sending it a list of byte values.

The I2C_Controller class has a simple .setup method which will guide the user through scanning and selecting a bus and selecting an address. Alternatively, a user can simply use the .save_data method to define these manually. The .save_data method allows one to save any data to the .data attribute which is a dictionary of any user defined values. The specific keys "bus" "address" "register" and "offset" will be saved in .data as well as given their own attributes which are used for the .read and .write methods. Data_Packets may also be saved. Data saved to the .data attribute may be retrived manually with a dictionary call or using the .report method. The .report method allows printing either specified data of "ALL" data to either the console or to a specified file. However, since the __repr__ method for Data_Packets is overloaded, any notes saved as attributes to the Data_Packet will not be reproduced in this manner.

The I2C_Controller of course has methods to .read and .write to the designated bus. If routing data has already been saved to the correct attributes or with the .save_data method, then the routing information does not need to be supplied. On can simply say what packet to .write or how many bytes to .read from the known bus, address, register.

The I2C_Controller also has a method to test a range of values on a given register, using a supplied data packet as a baseline. Want to verify that register 7 is red? Try something like the following:

    black = Data_Packet(bytes="0x01 0x00 0x00 0x00 0x00 0x02 0x00 0x00 0x00 0x00 0x64 0x64 0x00 0x00 0x01")
    
    my_controller.test_register(7, [0,255], black)

This will write all integer values from 0 to 255 (inclusive) on register 7 with the rest of the "black" data packet to fill it in. Just watch the card and see if it gets brighter red. You can also add a delay between each write to make it easier to see what's happening, or if your setup chokes with writing too fast.

This is by no means a finished product, and I don't know what else (if anything) I might be doing with this. Documentation isn't great, but I tried to do workable job. Most things should provide something useful with the Python help function. For example, the following and its output:

    help(I2C_Controller.report)


  

	Outputs saved data.

	Parameters
	----------
	*requested_data : string(s)
		Refers to the desired data output from I2C_Controller.data
		Call method without arguments to see availble data.

	**output_file_name : string
		Optional name of output file.
		Creates file if it does not already exists.
		Appends requested data to file.
		If no output is specified, data is printed to console.

	Examples
	----------
	my_controller.report("what", goes="where")
		Writes data what to file where
	my_controller.report("this","that" is_going="/over/here.txt")
		Writes data this and that to here.txt located in directory /over/
	my_controller.report("ALL")
		Prints all data from my_controller.data to console


Suggestions for improvements would be more than welcome. Hopefully somebody else can find this helpful.
